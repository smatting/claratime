import main
import datetime

def program():
    now = datetime.datetime.now()
    then = now - datetime.timedelta( minutes = 10 )
    ta = main.TimeAnimation( then, now )
    print ta.run()

if __name__ == '__main__':
    program()
