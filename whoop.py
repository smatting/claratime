import wiringpi2
import time

def silence():
    wiringpi2.wiringPiSetup()
    wiringpi2.pinMode( 1, wiringpi2.GPIO.INPUT )

def beep():
    wiringpi2.wiringPiSetup()
    wiringpi2.pinMode( 1, wiringpi2.GPIO.PWM_OUTPUT )
    wiringpi2.pwmSetMode( wiringpi2.GPIO.PWM_MODE_MS )

    for i in range( 10 ):
        wiringpi2.pinMode( 1, wiringpi2.GPIO.PWM_OUTPUT )
        wiringpi2.pwmSetMode( wiringpi2.GPIO.PWM_MODE_MS )

        wiringpi2.pwmSetRange( 1000 )
        wiringpi2.pwmWrite( 1, 500 )
        time.sleep( 50. * 1e-3 )

        wiringpi2.pinMode( 1, wiringpi2.GPIO.INPUT )

        time.sleep( 1000. * 1e-3 )


if __name__ == '__main__':
    silence()
