#!/usr/bin/env python
import datetime
import random
import time
import threading
import wiringpi2
import sys
import cPickle
import os
import sys
import math
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
from pkg_resources import resource_filename

filename_settings = resource_filename( __name__, 'settings.pkl' )

lcd = None

def today_at ( time, now = None ):
    if now is None:
        now = datetime.datetime.now()
    return datetime.datetime( now.year, now.month, now.day, time.hour, time.minute, 0 )

def tomorrow_at ( time, now = None ):
    if now is None:
        now = datetime.datetime.now()
    return today_at( time, now = now ) + datetime.timedelta( days = 1 )

class Settings ( object ):
    def __init__ ( self ):
        self.offset                   = datetime.timedelta( minutes = 10 )
        self.time_begin_uncertain     = datetime.time( 4, 0 )
        self.time_begin_reveal        = datetime.time( 18, 0 )
        self.timedelta_reveal_timeout = datetime.timedelta( hours = 3 )
        self.time_alarm               = datetime.time( 9, 30 )
        self.backlight_timeout        = datetime.timedelta( hours = 1 )

# only for demo
        self.chance                   = True

    def save ( self ):
        with open( filename_settings, 'w' ) as f:
            cPickle.dump( self, f )

    @staticmethod
    def load ():
        if os.path.isfile( filename_settings ):
            with open( filename_settings, 'r' ) as f:
                return cPickle.load( f )
        else:
            return Settings()

class Bell ( object ):
    def __init__ ( self ):
        self.event_silence = None

    def ring ( self ):
        self.event_silence = threading.Event()
        thread = threading.Thread( target = self.ringing )
        thread.start()

    def stop ( self ):
        if self.event_silence is not None:
            self.event_silence.set()

    def ringing_mute ( self ):
        while not self.event_silence.is_set():
            print 'beep...'
            sys.stdout.flush()
            time.sleep( 1000. * 1e-3 )

    def ringing ( self ):
        wiringpi2.pinMode( 1, wiringpi2.GPIO.PWM_OUTPUT )
        wiringpi2.pwmSetMode( wiringpi2.GPIO.PWM_MODE_MS )

        while not self.event_silence.is_set():
            wiringpi2.pinMode( 1, wiringpi2.GPIO.PWM_OUTPUT )
            wiringpi2.pwmSetMode( wiringpi2.GPIO.PWM_MODE_MS )

            wiringpi2.pwmSetRange( 1000 )
            wiringpi2.pwmWrite( 1, 500 )
            time.sleep( 50. * 1e-3 )

            wiringpi2.pinMode( 1, wiringpi2.GPIO.INPUT )

            time.sleep( 1000. * 1e-3 )

        wiringpi2.pinMode( 1, wiringpi2.GPIO.INPUT )
        self.event_silence.clear()
        self.event_silence = None

class UncertainTime ( object ):
    states = [ 'DEFAULT', 'TIME_OFFSET' ]

    def __init__ ( self, settings ):
        self.settings = settings
        self.offset   = datetime.timedelta(0)
        self.state    = 'DEFAULT'

    def now ( self, now = None ):
        if now is None:
            now = datetime.datetime.now()
        if self.state == 'TIME_OFFSET':
            return now + self.offset
        else:
            return now

    def on_tick ( self, now = None ):
        if now is None:
            now = datetime.datetime.now()

        dtime_begin_uncertain = today_at( self.settings.time_begin_uncertain, now = now )
        dtime_begin_reveal    = today_at( self.settings.time_begin_reveal, now = now )
        dtime_end_reveal      = dtime_begin_reveal + self.settings.timedelta_reveal_timeout

        if self.state == 'DEFAULT':
            if ( now < dtime_end_reveal ) and ( now >= dtime_begin_uncertain ):
                self.enter_state( 'TIME_OFFSET' )
                return

        if self.state in [ 'TIME_OFFSET', 'USER_REVEALED' ]:
            if now >= dtime_end_reveal:
                self.enter_state( 'DEFAULT' )
                return

        # if now >= dtime_end_reveal:
        #     self.reveal( by_user = False )
        #     #elif self.state == 'TIME_OFFSET':

    def reveal ( self ):
        self.enter_state( 'USER_REVEALED' )

    def enter_state ( self, state ):
        print 'entering {0}'.format( state )
        if state == 'TIME_OFFSET':
            if self.settings.chance:
            #if random.random() > 0.5:
                self.offset = self.settings.offset
            else:
                self.offset = datetime.timedelta(0)
        self.state = state

    def exit_state ( self ):
        pass

    def reveal_possible ( self, now = None ):
        '''
        True if possible and makes sense
        '''
        if now is None:
            now = datetime.datetime.now()

        dtime_begin_reveal    = today_at( self.settings.time_begin_reveal, now = now )
        dtime_end_reveal      = dtime_begin_reveal + self.settings.timedelta_reveal_timeout
        return ( self.state == 'TIME_OFFSET' ) and \
               ( now >= dtime_begin_reveal ) and \
               ( now < dtime_end_reveal )

class Menu ( object ):
    def __init__ ( self, clara_clock ):
        self.clara_clock = clara_clock
        self.menu_structure = { 
                'dummy' : [ 'root' ],
                'root': [ 'alarm', 'tmachine', 'reboot', 'demo' ],
                'tmachine' : [ 'tmachine_set' ],
                'demo' : [ 'schade', 'yay' ],
                'alarm' : [ 'alarm_on_off', 'alarm_time' ]
                }
        self.node_current = None
        self.lm_current = None
        self.node_selected = None

    def label ( self, menu_node ):
        labels = {
                'alarm' : 'Wecker',
                'demo' : 'Demo',
                'reboot' : 'Neustart',
                'tmachine' : 'Zeitmaschine',
                'schade' : 'Schade',
                'yay' : 'Yay!'
                }
        if menu_node in labels:
            return labels[ menu_node ]

        if menu_node == 'alarm_on_off':
            if self.clara_clock.alarm.state == 'WAITING':
                return 'Wecker: an'
            else:
                return 'Wecker: aus'

        if menu_node == 'alarm_time':
            time = self.clara_clock.settings.time_alarm
            return 'Zeit: {0:02}:{1:02}'.format( time.hour, time.minute )

        if menu_node == 'tmachine_set':
            time = self.clara_clock.settings.time_begin_reveal
            return 'Knopf: {0:02}:{1:02}'.format( time.hour, time.minute )

        return 'xxxx'

    def parent_node ( self, node ):
        for parent_maybe in self.menu_structure:
            if node in self.menu_structure[parent_maybe]:
                return parent_maybe
        return None

    def make_menu ( self, node ):
        m = self.menu_structure.get( node, [] )
        entries = [ self.label( k ) for k in m ]
        lm = ListMenu( entries )
        return lm

    def start_menu ( self, node ):
        self.node_current = node
        self.lm_current = self.make_menu( node )
        self.lm_current.display()

    def refresh_labels ( self ):
        if self.lm_current is not None:
            node = self.node_current
            m = self.menu_structure.get( node, [] )
            entries = [ self.label( k ) for k in m ]
            self.lm_current.entries = entries
            self.lm_current.display()

    def on_btn ( self, btn ):
        if self.lm_current is not None:
            if btn == lcd.LEFT:
                pn = self.parent_node( self.node_current )

                if pn == 'dummy':
                    self.node_selected = 'dummy'
                    return

                if pn is not None:
                    self.start_menu( pn )
            else:
                self.lm_current.on_btn( btn )

                if self.lm_current.selection is not None:
                    m = self.menu_structure[ self.node_current ]
                    node_selected = m[ self.lm_current.selection ]
                    if node_selected in self.menu_structure:
                        self.start_menu( node_selected )
                    else:
                        self.node_selected = node_selected
                    self.lm_current.selection = None

class ListMenu ( object ):
    def __init__ ( self, entries ):
        if entries == None:
            entries = []

        self.entries = entries

        self.window_begin = 0
        self.position = 0
        self.selection = None
        self.selection_last = None

    def display ( self ):
        lcd.clear()
        n = len( self.entries )
        pos = [ self.window_begin, ( self.window_begin + 1 ) % n ]

        if len( self.entries ) < 2:
            pos = pos[:-1]

        pointer_char = { False: ' ', True: '~' }
        lines = []
        for j in pos:
            s = '{0} {1:14}'.format( pointer_char[ j == self.position ], self.entries[j] )
            lines.append( s )

        lcd.clear()
        lcd.message( '\n'.join( lines ) )

    def on_btn ( self, btn ):
        delta = 0
        if btn == lcd.UP:
            delta = -1
        elif btn == lcd.DOWN:
            delta = 1
        elif btn == lcd.RIGHT or btn == lcd.SELECT:
            self.selection = self.position
        else:
            return

        n = len( self.entries )

        i = self.position
        i = ( i + delta ) % n

        j = self.window_begin
        if ( ( j - 1 ) % n == i ) and delta == -1:
            j = ( j - 1 ) % n
        elif ( ( j + 2 ) % n == i ) and delta == 1:
            j = ( j + 1 ) % n

        self.position = i
        self.window_begin = j

        self.display()

class Alarm ( object ):
    def __init__ ( self, settings, backlight ):
        self.settings = settings
        self.backlight = backlight
        self.state = 'OFF'
        self.alarm_datetime = None
        self.bell = Bell()

    def on_tick ( self, now ):
        if self.state == 'WAITING':
            if self.alarm_datetime is not None:
                if now >= self.alarm_datetime:
                    self.backlight.set_on()
                    self.bell.ring()
                    self.state = 'RINGING'

    def set_on ( self, now ):
        self.state = 'WAITING'
        alarm_today = today_at( self.settings.time_alarm, now = now )
        alarm_tomorrow = tomorrow_at( self.settings.time_alarm, now = now )
        if alarm_today > now:
            self.alarm_datetime = alarm_today
        else:
            self.alarm_datetime = alarm_tomorrow

    def set_off ( self ):
        self.bell.stop()
        self.state = 'OFF'

    def snooze ( self, now ):
        if self.state == 'RINGING':
            print 'trying to  stop'
            self.bell.stop()
            self.alarm_datetime = now + datetime.timedelta( minutes = 9 )
            self.state = 'WAITING'

class KeyPad ( object ):
    def __init__ ( self ):
        self.btn_down = None
        self.btn_down_repeat = None
        self.btn_up = None

        self.buttons = [ lcd.SELECT,
                         lcd.LEFT,
                         lcd.UP,
                         lcd.DOWN,
                         lcd.RIGHT ]

    def on_tick ( self ):
        for btn in self.buttons:
            if lcd.buttonPressed(btn):
                if btn == self.btn_down or btn == self.btn_down_repeat:
                    self.btn_down_repeat = btn
                    self.btn_down = None
                    self.btn_up = None
                    return
                else:
                    self.btn_down = btn
                    self.btn_down_repeat = None
                    self.btn_up = None
                    return

        if self.btn_down is not None:
            self.btn_up
        else:
            self.btn_up = self.btn_down_repeat
        self.btn_down = None
        self.btn_down_repeat = None
        return

class BackLight ( object ):
    def __init__ ( self, settings ):
        self.settings = settings
        self.set_on()
        self.dt_on = None

    def set_on ( self ):
        self.state = 'ON'
        self.dt_on = datetime.datetime.now()
        lcd.backlight( lcd.VIOLET )

    def set_off ( self ):
        self.state = 'OFF'
        lcd.backlight( lcd.OFF )

#    def toggle ( self ):
#        if self.state == 'ON':
#            self.set_off()
#        else:
#            self.set_on()

    def on_tick ( self ):
        now = datetime.datetime.now()
        if self.dt_on is not None:
            delta = now - self.dt_on
            if delta >= self.settings.backlight_timeout:
                self.set_off()

class TimeSelect ( object ):
    def __init__ ( self, name, time = None ):
        self.name = name
        if time == None:
            time = datetime.time( 0, 0 )
        self.time = time
        self.position = 0
        self.time_selected = None

    def display ( self ):
        s = '     {0:02}:{1:02}'.format( self.time.hour, self.time.minute )
        lcd.clear()
        lcd.message( s )
        col_at_pos = [ 6, 9 ]
        col = col_at_pos[ self.position ]
        lcd.cursor()
        lcd.setCursor( col, 0 )

    def on_btn ( self, btn ):
        if btn in [ lcd.LEFT, lcd.RIGHT ]:
            self.position = 1 - self.position
            self.display()

        if btn in [ lcd.UP, lcd.DOWN ]:
            if self.position == 0:
                h = self.time.hour
                delta = { lcd.UP: 1, lcd.DOWN: -1 }
                h = ( h + delta[btn] ) % 24
                self.time = datetime.time( h, self.time.minute )

            if self.position == 1:
                m = self.time.minute
                delta = { lcd.UP: 5, lcd.DOWN: -5 }
                #delta = { lcd.UP: 1, lcd.DOWN: -1 }
                m = ( m + delta[btn] ) % 60
                self.time = datetime.time( self.time.hour, m )

            self.display()

        if btn == lcd.SELECT:
            self.time_selected = self.time

def query_time ( time_init = None ):
    if time_init is None:
        time_init = datetime.time( 15, 0 )
    ts = TimeSelect( name = 'foo', time = time_init )

    keypad = KeyPad()

    while ts.time_selected is None:
        keypad.on_tick()
        ts.on_btn( keypad.btn_down )
        time.sleep(0.05)

    return ts.time_selected

def beep ( range = 1000, duration = 20. ):
    wiringpi2.pinMode( 1, wiringpi2.GPIO.PWM_OUTPUT )
    wiringpi2.pwmSetMode( wiringpi2.GPIO.PWM_MODE_MS )
    wiringpi2.pwmSetRange( range )
    wiringpi2.pwmWrite( 1, 500 )
    time.sleep( duration * 1e-3 )
    wiringpi2.pinMode( 1, wiringpi2.GPIO.INPUT )

class TimeAnimation ( object ):
    def __init__ ( self, clara_clock, dt_a, dt_b ):
        '''
        assuming time_a <= time_b
        '''
        self.clara_clock = clara_clock
        self.dt_a = dt_a
        self.dt_b = dt_b
        self.duration = 3.0

        wiringpi2.pinMode( 1, wiringpi2.GPIO.PWM_OUTPUT )
        wiringpi2.pwmSetMode( wiringpi2.GPIO.PWM_MODE_MS )

    def run ( self ):
        minutes = ( self.dt_b - self.dt_a ).seconds / 60

        # in seconds
        time_begin = time.time()
        time_last = time.time()

        m_last = None

        t = ( time.time() - time_begin ) / self.duration
        while t <= 1.0:
            #p = -t*(t-2)

            #t2 = t - 1.0
            #p = t2*t2*t2 + 1

            t2 = t * 0.9
            p = -math.pow( 2, -10 * t2  ) + 1

            m = math.floor( minutes * p  )
            if m != m_last:
                m_last = m
                dt = self.dt_a + datetime.timedelta( minutes = ( minutes - m ) )
                self.clara_clock.display_time( dt, force = True )
                beep()

            t = ( time.time() - time_begin ) / self.duration
            time.sleep( 0.05 )
        beep()
        time.sleep( 0.1 )

class ClaraClock ( object ):
    def __init__ ( self ):
        global lcd
        wiringpi2.wiringPiSetup()
        lcd = Adafruit_CharLCDPlate(busnum=1)
        lcd.begin(16, 2)
        lcd.clear()

        self.settings = Settings.load()

        self.time_displayed = datetime.time( 0, 0 )

        self.backlight = BackLight( self.settings )
        self.utime = UncertainTime( self.settings )
        self.alarm = Alarm( self.settings, self.backlight )
        self.keypad = KeyPad()

        self.backlight.set_on()

        self.menu = Menu( self )

        self.menu_active = False

        self.time_select = None

    def main_loop ( self ):
        while True:
            self.backlight.on_tick()
            self.utime.on_tick()
            now = self.utime.now()
            self.alarm.on_tick( now )
            self.keypad.on_tick()

            if not self.menu_active:
                self.display_time( )

            self.process_buttons()
            time.sleep(0.05)

    def process_buttons ( self ):
        if self.keypad.btn_down is None:
            return

        if self.backlight.state == 'OFF':
            self.backlight.set_on()
            return

        if self.menu_active:
            self.menu.on_btn( self.keypad.btn_down )

            if self.menu.node_selected is not None:
                node = self.menu.node_selected 
                self.on_menu( node )
                self.menu.node_selected = None
                return

            return

        if self.alarm.state == 'RINGING':
            print 'trying to snooze'
            self.alarm.snooze( now = self.utime.now() )
            return

        if self.utime.reveal_possible():
            if self.keypad.btn_down == lcd.LEFT:
                if self.utime.offset >= datetime.timedelta( minutes = 1 ):
                    lcd.backlight( lcd.GREEN )

                    now_real = datetime.datetime.now()
                    now_with_offset = self.utime.now()
                    animation = TimeAnimation( self, now_real, now_with_offset )
                    self.utime.reveal()
                    animation.run()
                    self.display_time( force = True )

                    self.backlight.set_on()
                else:
                    self.utime.reveal()
                    self.display_time( force = True )
                    lcd.backlight( lcd.RED )
                    beep( range = 2000, duration = 100 )
                    time.sleep( 0.1 )
                    beep( range = 2000, duration = 100 )
                    time.sleep(1)
                    self.backlight.set_on()


        # general

        if self.keypad.btn_down == lcd.UP:
            self.backlight.set_off()

        if self.keypad.btn_down == lcd.RIGHT:
            self.menu_active = True
            self.menu.start_menu( 'root' )

    def on_menu ( self, node ):
        if node == 'reboot':
            sys.exit(0)

        if node == 'dummy':
            self.menu_active = False
            self.display_time( force = True )

        if node == 'alarm_on_off':
            if self.alarm.state == 'OFF':
                self.alarm.set_on( now = self.utime.now() )
            else:
                self.alarm.set_off()
            self.menu.refresh_labels()

        if node == 'alarm_time':
            time = query_time( self.settings.time_alarm )
            self.settings.time_alarm  = time
            self.settings.save()
            self.menu.refresh_labels()
            if self.alarm.state == 'WAITING':
                self.alarm.set_on( now = self.utime.now()  )

        if node == 'animation':
            now = datetime.datetime.now()
            then = now - datetime.timedelta( minutes = 10 )
            animation = TimeAnimation( self, then, now )
            animation.run()
            self.display_time( force = True )
            self.menu_active = False

        if node == 'tmachine_set':
            time = query_time( self.settings.time_begin_reveal )
            self.settings.time_begin_reveal = time
            self.settings.save()
            self.menu.refresh_labels()

        if node == 'schade':

            now = datetime.datetime.now()
            td = datetime.timedelta( minutes = 1 )

            self.settings.offset                   = datetime.timedelta( minutes = 10 )
            self.settings.time_begin_uncertain     = ( now - 2*td ).time()
            self.settings.time_begin_reveal        = ( now - 1*td ).time()
            self.settings.timedelta_reveal_timeout = datetime.timedelta( hours = 3 )
            self.settings.time_alarm               = datetime.time( 9, 30 )
            self.settings.backlight_timeout        = datetime.timedelta( hours = 1 )
            self.settings.chance                   = False

            self.settings.save()

            self.utime.enter_state( 'DEFAULT' )

        if node == 'yay':

            now = datetime.datetime.now()
            td = datetime.timedelta( minutes = 1 )

            self.settings.offset                   = datetime.timedelta( minutes = 10 )
            self.settings.time_begin_uncertain     = ( now - 2*td ).time()
            self.settings.time_begin_reveal        = ( now - 1*td ).time()
            self.settings.timedelta_reveal_timeout = datetime.timedelta( hours = 3 )
            self.settings.time_alarm               = datetime.time( 9, 30 )
            self.settings.backlight_timeout        = datetime.timedelta( hours = 1 )
            self.settings.chance                   = True

            self.settings.save()

            self.utime.enter_state( 'DEFAULT' )

    def display_time ( self, now = None, force = False ):
        if now is None:
            now = self.utime.now()
        t = datetime.time( now.hour, now.minute )
        if t != self.time_displayed or force:
            self.time_displayed = t
            lcd.clear()
            indicator_alarm  = { 'WAITING' : '.' }.get( self.alarm.state, ' ' )
            indicator_reveal = { True : '<?' }.get( self.utime.reveal_possible(), '  ' )
            lcd.message( '{2}    {0:02}:{1:02}    {3}'.format( t.hour, t.minute, indicator_alarm, indicator_reveal ) )

if __name__ == '__main__':
    clock = ClaraClock()
    clock.main_loop()
